from app.models.book_model import database
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqldb = database()
@jwt_required()
def shows():
    dbresult = mysqldb.showsBook()
    result = []
    for item in dbresult:
        book = {
            "bookid" :item[0],
            "nama" :item[1],
            "pengarang" :item[2],
            "tahunterbit" :item[3],
            "genre" :item[4]
        }
        result.append(book)
    return jsonify(result)

# @jwt_required()
def showBookById(params):
    dbresult = mysqldb.showBookById(params)
    data = {
        "bookid" :dbresult[0],
        "nama" :dbresult[1],
        "pengarang" :dbresult[2],
        "tahunterbit" :dbresult[3],
        "genre" :dbresult[4]
    }
    return jsonify(data)

@jwt_required()
def insert(**params):
    mysqldb.insertBook(**params)
    mysqldb.dataCommit()
    return jsonify({"Message":"Success"})