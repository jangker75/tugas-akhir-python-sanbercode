from app.controllers import book_controller
from app.models.borrow_model import database
from app.models.customer_model import database as cus_db
from app.controllers import book_controller
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime
import requests
mysqldb = database()
cus_db = cus_db()

@jwt_required()
def shows():
    params = get_jwt_identity()
    dbresult = mysqldb.showBorrowByEmail(**params)
    result = []
    if dbresult is not None:
        for item in dbresult:
            id = item[4]
            bookdetail = book_controller.showBookById(id).data
            user = {
                "username" :item[0],
                "borrowid" :item[1],
                "borrowdate" :item[2],
                "bookid" :item[4],
                "bookname" :item[5],
                "author" :bookdetail[2],
                "releaseyear" :bookdetail[3],
                "genre" :bookdetail[4]
            }
            result.append(user)
    else:
        result = dbresult
    return jsonify(result)

@jwt_required()
def insert(**params):
    token = get_jwt_identity()
    userid = cus_db.showUserByEmail(**token)[0]
    borrowdate = datetime.datetime.now().isoformat()
    id = json.dumps({"id":params["bookid"]})
    bookname = getBookById(id)["nama"]
    params.update(
        {
            "userid":userid,
            "borrowdate":borrowdate,
            "bookname":bookname,
            "isactive":1
        }
    )
    mysqldb.insertBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

@jwt_required()
def changeStatus(**params):
    mysqldb.updateBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def getBookById(data):
    book_data = requests.get(url="http://localhost:5000/book", params=data)
    return book_data.json()