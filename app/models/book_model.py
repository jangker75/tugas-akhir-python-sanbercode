from mysql.connector import connect


class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='perpustakaan',
                              user='root',
                              password='Somay231')
        except Exception as e:
            print(e)

    def showsBook(self):
        try:
            cursor = self.db.cursor()
            query = '''select * from books'''
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            print (e)
    
    def showBookById(self, params):
        try:
            cursor = self.db.cursor()
            query = '''select * from books
            where bookid = {0};'''.format(params)
            cursor.execute(query)
            result = cursor.fetchone()
            # print(list(result))
            return result
        except Exception as e:
            print(e)

    def insertBook(self, **params):
        try:
            column = ', '.join(list(params.keys()))
            values = tuple(list(params.values()))
            crud_query = '''insert into books ({0}) values {1};'''.format(column, values)
            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)
    
    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
    
    def closeConnection(self):
        if self.db is not None and self.db.is_connected():
            self.db.close()