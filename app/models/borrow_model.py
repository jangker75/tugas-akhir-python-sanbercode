from mysql.connector import connect


class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
            database='perpustakaan',
            user='root',
            password='Somay231')
        except Exception as e:
            print(e)
    
    def showBorrowByEmail(self, **params):
        try:
            cursor = self.db.cursor()
            query = '''select cs.username, br.*
            from borrows as br
            inner join customers as cs on br.userid = cs.userid
            where cs.email = "{0}" and br.isactive = 1
            ;'''.format(params['email'])
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            print(e)

    def insertBorrow(self, **params):
        try:
            column = ', '.join(list(params.keys()))
            values = tuple(list(params.values()))
            crud_query = '''
            insert into borrows
            ({0}) values {1}
            ;'''.format(column, values)
            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)

    def updateBorrow(self, **params):
        try:
            borrowid = params['borrowid']
            crud_query = '''
            update borrows
            set isactive=0 where borrowid = {0}
            ;'''.format(borrowid)
            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)

    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
    
    def closeConnection(self):
        if self.db is not None and self.db.is_connected():
            self.db.close()