from flask.json import jsonify
from app import app
from app.controllers import book_controller
from flask import Blueprint, request

books_blueprint = Blueprint("books_router", __name__)

@app.route("/books", methods=["GET"])
def showBooks():
    return book_controller.shows()

@app.route("/book", methods=["GET"])
def showBook():
    param = request.args.get('id')
    return book_controller.showBookById(param)

@app.route("/book/insert", methods=["POST"])
def insertBook():
    param = request.json
    return book_controller.insert(**param)

