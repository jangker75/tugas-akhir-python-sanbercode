from app import app
from app.controllers import borrow_controller
from flask import Blueprint, request

borrow_blueprint = Blueprint("borrows_router", __name__)


@app.route("/borrows", methods=["GET"])
def showBorrow():
    return borrow_controller.shows()

@app.route("/borrows/insert", methods=["POST"])
def insertBorrow():
    params = request.json
    return borrow_controller.insert(**params)

@app.route("/borrows/status", methods=["POST"])
def statusBorrow():
    return borrow_controller.changeStatus()

