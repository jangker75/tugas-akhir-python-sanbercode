from app import app
from app.controllers import customer_controller
from flask import Blueprint, request

customers_blueprint = Blueprint("customers_router", __name__)

@app.route("/users", methods=["GET"])
def showUsers():
    return customer_controller.shows()

@app.route("/user", methods=["POST"])
def showUser():
    param = request.json
    return customer_controller.show(**param)

@app.route("/user/insert", methods=["POST"])
def insertUser():
    param = request.json
    return customer_controller.insert(**param)

@app.route("/user/update", methods=["POST"])
def updateUser():
    param = request.json
    return customer_controller.update(**param)

@app.route("/user/delete", methods=["POST"])
def deleteUser():
    param = request.json
    return customer_controller.delete(**param)

@app.route("/reqtoken", methods=["POST"])
def reqToken():
    param = request.json
    return customer_controller.token(**param)
