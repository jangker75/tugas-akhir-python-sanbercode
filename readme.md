# Aplikasi Perpustakaan

Aplikasi untuk mengelola database mysql mengenai data buku dan peminjaman buku pada perpustakaan sederhana

## Requirement

* Python versi 3.9.5
* Database mysql

## Cara penggunaan

- buat database baru dengan nama "perpustakaan"
- upload db.sql ke dalam database perpustakaan
- edit file username dan password pada file model di semua folder model
- running di command prompt `python main.py`
- akses API dengan postman

## Package yang digunakan

* Flask (Modul untuk webserver)
* JWT (Modul untuk manajemen Api Token)
* Json (Modul untuk parse data json)
* Datetime (Modul untuk mengambil waktu sekarang)
* Mysql-connector-python (Modul untuk koneksi ke database mysql)
Daftar package pendukung lainnya dapat dilihat pada file requirements.txt

## REST API
Aplikasi ini menggunakan banyak API untuk memanajemen database perpustakaan. dapat menggunakan postman untuk mengakses REST API ini

#### Daftar url

* `/reqtoken (POST)` meminta access token untuk meminta data
* `/users (GET)` menampilkan seluruh data customer
* `/user (POST)` menampilkan satu data customer
* `/user/insert (POST)` memasukkan data customer 
* `/user/update (POST)` merubah data customer 
* `/user/delete (POST)` menghapus data customer 

* `/borrows (GET)` Menampilkan seluruh data peminjaman dengan parameter email yang diambil dari akses token
* `/borrows/insert (POST)` memasukkan data peminjaman
* `/borrows/status (POST)` merubah data status peminjaman

* `/books (GET)` Menampilkan seluruh data Buku